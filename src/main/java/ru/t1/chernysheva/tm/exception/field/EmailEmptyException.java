package ru.t1.chernysheva.tm.exception.field;

public class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException () {
        super("Error! Email is empty...");
    }

}
