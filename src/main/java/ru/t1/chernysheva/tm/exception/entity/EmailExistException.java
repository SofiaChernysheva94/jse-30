package ru.t1.chernysheva.tm.exception.entity;

public class EmailExistException extends AbstractEntityException {

    public EmailExistException() {
        super("Error! Login is not exist...");
    }

}
