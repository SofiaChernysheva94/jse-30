package ru.t1.chernysheva.tm.component;

import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public final class FileScanner extends Thread {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @Nullable
    private Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        start();
    }

    @SneakyThrows
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            for (@NotNull final File file : folder.listFiles()) {
                if (file.isDirectory()) continue;
                @NotNull final String fileName = file.getName();
                String fileNameWithoutExt = FilenameUtils.removeExtension(fileName);
                final boolean check = commands.contains(fileNameWithoutExt);
                if (check) {
                    try {
                        file.delete();
                        bootstrap.processCommand(fileNameWithoutExt);
                    } catch (@NotNull final Exception e) {
                        bootstrap.getLoggerService().error(e);
                    }
                }
            }
        }
    }

}
