package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.model.User;

public interface IUserService extends IService<User>{

    @Nullable
    User create(@Nullable final String login, @Nullable final String password);

    @Nullable
    User create(@Nullable final String login, @Nullable String password, @Nullable final String email);

    @NotNull
    User create(@Nullable final String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    void removeByLogin(@Nullable final String login);

    @Nullable
    User removeByEmail(@Nullable final String email);

    void setPassword(@Nullable String id, @Nullable String password);

    void updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @Nullable
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
